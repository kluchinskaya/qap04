class HumanClassLimit:

    def walk(self, steps_passed):
        if steps_passed > 5:                                # 'if - else' statement was chosen
            print("Sorry. I can't walk so much")            # to resolve, what output should be
        else:                                               # depending on provided value in 'steps_passed'
            print("I went " + str(steps_passed) + " kms")