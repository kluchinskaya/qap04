# A cube is given. It has a volume and a square
# Only integer numbers are allowed to be provided
edge = 7
print("The volume of the cube is", int(edge)**3, "cubic meters")
print("Total square of the cube is", int(edge)**2*6, "square meters")

# example with user input below, just replace row 3 with the following:
edge = input("Enter the edge of the cube: ")
